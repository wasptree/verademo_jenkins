pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                // Compile Java app
                sh 'mvn clean package'
                // pull docker container
                //sh 'doker pull juliantotzek/verademo1-tomcat'
            }
        }
        stage('Security Scan Master Branch') {
            when {
                branch 'master'
            }
            steps {
                parallel(
                    a:{
                        // Policy scan
                        withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODEKEY', usernameVariable: 'VERACODEID')]) {
                            veracode applicationName: "Verademo_Jenkins", criticality: 'High',
                            fileNamePattern: '', replacementPattern: '', scanExcludesPattern: '', scanIncludesPattern: '',
                            createProfile: true,
                            canFailJob: true,
                            scanName: 'build $buildnumber - Jenkins Pipeline',
                            uploadExcludesPattern: '', uploadIncludesPattern: 'target/*.war', waitForScan: true, timeout: 30,
                            vid: VERACODEID, vkey: VERACODEKEY
                        }
                    },
                    b:{
                        // SCA Scan
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh'
                        }
                    },
                    c:{
                        // Container / IAC Scan
                        catchError(buildResult: 'SUCCESS', stageResult: 'UNSTABLE', message: 'Veracode Policy Failure') {
                            withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODE_API_KEY_SECRET', usernameVariable: 'VERACODE_API_KEY_ID')]) {
                                sh '  curl -fsS https://tools.veracode.com/veracode-cli/install | sh || true '
                                sh '  ./veracode scan --type directory --source ./ --format table --output iac-scan.table   '
                                sh '  grep "Policy Passed" iac-scan.table && grep "Policy Passed = true" iac-scan.table'
                            }
                        }
                    },
                )
            }
        }
        stage('Security Scan Feature Branch'){
            when {
                branch 'feature'
            }
            steps {
                parallel(
                    a:{
                        // Sandbox scan
                        withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODEKEY', usernameVariable: 'VERACODEID')]) {
                            veracode applicationName: "Verademo_Jenkins", criticality: 'High', createSandbox: true, sandboxName: "jenkins-release", 
                            fileNamePattern: '', replacementPattern: '', scanExcludesPattern: '', scanIncludesPattern: '',
                            scanName: 'build $buildnumber - Jenkins Pipeline',
                            uploadExcludesPattern: '', uploadIncludesPattern: 'target/*.war', waitForScan: true, timeout: 30,
                            vid: VERACODEID, vkey: VERACODEKEY
                        }
                    },
                    b:{
                        // 3rd party scan application
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh'
                        }
                    },
                )
            }
        }
        stage('Security Scan Development Branch'){
            when {
                branch 'development'
            }
            steps{
                parallel(
                    a:{
                        //Pipeline scan
                        withCredentials([usernamePassword(credentialsId: 'VeracodeAPI', passwordVariable: 'VERACODEKEY', usernameVariable: 'VERACODEID')]) {
                            sh 'curl -O https://downloads.veracode.com/securityscan/pipeline-scan-LATEST.zip'
                            sh 'unzip -o pipeline-scan-LATEST.zip pipeline-scan.jar'
                            sh 'java -jar pipeline-scan.jar -vid "$VERACODEID" -vkey "$VERACODEKEY" --request_policy "Jenkins_SAST_SCA"'
                            sh 'java -jar pipeline-scan.jar -vid "$VERACODEID" -vkey "$VERACODEKEY" --file target/verademo.war --policy_file "Jenkins_SAST_SCA.json"'
                        }
                    },
                    b:{
                        // 3rd party scan application
                        withCredentials([string(credentialsId: 'SRCCLR_API_TOKEN', variable: 'SRCCLR_API_TOKEN')]) {
                            sh 'curl -sSL https://download.sourceclear.com/ci.sh | sh'
                        }
                    },
                )
            }
        }
    }
}

